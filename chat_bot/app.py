from flask import Flask, render_template, jsonify, request
import requests
import json
import os

app = Flask(__name__)

URL = os.environ.get('YOUR_API_URL')
ENDPOINT_KEY = os.environ.get('YOUR_ENDPOINT_KEY')

@app.route("/")
def home():
    """Serve the homepage."""
    return render_template('index.html')

@app.route("/ask", methods=['GET'])
def ask():
    """Handle Q&A requests."""
    text = request.args.get('question', '')
    
    if not text:
        return jsonify({'error': 'No question provided'}), 400

    headers = {
        'Authorization': f'EndpointKey {ENDPOINT_KEY}',
        'Content-Type': 'application/json',
    }

    data = json.dumps({'question': text})

    try:
        response = requests.post(URL, headers=headers, data=data)
        response.raise_for_status()
    except requests.exceptions.RequestException as e:
        return jsonify({'error': str(e)}), 500

    information = response.json()
    answer = information.get('answers', [{}])[0].get('answer', 'Sorry, I could not find an answer.')

    return jsonify({'answer': answer})

if __name__ == "__main__":
    app.run(debug=True)
