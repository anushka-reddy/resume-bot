function fetchServerResponse(question) {
    const host = window.location.host;
    const encodedQuestion = encodeURIComponent(question);
    return fetch(`https://${host}/ask/${encodedQuestion}`)
      .then(response => response.text());
}

function addMessageToChat(question, response) {
    const chatBox = document.getElementById('chat');
    const questionHTML = document.createElement('div');
    questionHTML.className = 'alert alert-success';
    questionHTML.textContent = question;

    const responseHTML = document.createElement('div');
    responseHTML.className = 'alert alert-secondary';
    responseHTML.textContent = response;

    chatBox.append(questionHTML, responseHTML);
    chatBox.scrollTop = chatBox.scrollHeight;
}

function clearQuestionInput() {
    document.getElementById('question').value = "";
}

function requestServer() {
    const question = document.getElementById('question').value.trim();

    if (question) {
        fetchServerResponse(question)
            .then(response => {
                addMessageToChat(question, response);
                clearQuestionInput();
            })
            .catch(error => {
                console.error('Error fetching the server response:', error);
            });
    } else {
        // Handle empty input, e.g., by showing a message to the user
        console.log('Please enter a question.');
    }
}

// Attach the requestServer function to button click using JavaScript instead of inline HTML event handlers
document.addEventListener('DOMContentLoaded', () => {
    const askButton = document.querySelector('button[type="button"]');
    askButton.addEventListener('click', requestServer);
});
