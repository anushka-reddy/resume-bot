# README for Chat Bot Interface Repository

## Overview

This repository contains a simple chat bot interface application designed to run on a web server. The application allows users to input questions, which are then sent to a server-side Flask application. The Flask app processes these questions, sends them to a specified external service (defined by `URL` and `ENDPOINT_KEY` in the Python script), and returns the service's response to the user through the web interface.

## Structure

The repository consists of the following files:

- `SCRIPT.JS`: Contains JavaScript functions for handling the user interface interactions, including sending the user's question to the server and displaying the server's response in the chat interface.
- `STYLE.CSS`: Provides the CSS styling for the chat interface, ensuring a user-friendly and responsive layout.
- `APP.PY`: A Flask application that serves the web interface and handles the communication between the web client and the external question-answering service.
- `INDEX.HTML`: The HTML template for the chat interface, which includes links to the Bootstrap framework for styling, as well as the custom CSS and JavaScript files.

## Features

- **User Question Input**: Allows users to type questions into a text box and submit them by clicking the "Ask!" button.
- **Dynamic Chat Interface**: Displays the user's questions and the bot's responses within a chat-like interface, providing a seamless interaction experience.
- **Error Handling**: Includes basic error handling for failed server responses or issues with the external service.
- **Responsive Design**: Utilizes CSS variables and Bootstrap for a layout that adapts to different screen sizes and devices.

## Example
![Chat Bot](./readme_images/chatbot.png "Chat Bot Interface Example")

## Installation

1. **Clone the Repository**

   Clone this repository to your local machine using your preferred method (SSH or HTTPS).

2. **Set Up a Python Environment**

   It's recommended to create a virtual environment for Python projects. Use the following command to create one:

   ```bash
   python -m venv venv
   ```

   Activate the virtual environment:

   - On Windows: `venv\Scripts\activate`
   - On Unix or MacOS: `source venv/bin/activate`

3. **Install Dependencies**

   Install the required Python packages using pip:

   ```bash
   pip install flask requests
   ```

4. **Configuration**

   Update the `APP.PY` file with your external service's URL and endpoint key. Replace `<your-url>` and `<your-endpoint>` with the actual Azure API values.

5. **Run the Application**

   Start the Flask application by running:

   ```bash
   python app.py
   ```

   Visit `http://localhost:5000` in your web browser to interact with the chat bot interface.

## Usage

Once the application is running, you can use it by simply typing a question into the input field on the web page and clicking the "Ask!" button. The chat interface will display your question followed by the bot's response.

## Contributing

Contributions to this project are welcome. Please fork the repository and submit a pull request with your proposed changes or improvements.
